module Main exposing (main)

import Array
import Browser
import Browser.Navigation as Nav
import File.Download
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onMouseOut, onMouseOver)
import Json.Encode as Encode
import Process
import Task
import Time exposing (Posix)
import Url


type Node
    = Header { title : String }
    | KCM { title : String }
    | Category { title : String, children : List Node }


kcm : String -> Node
kcm val =
    KCM { title = val }


category : String -> List Node -> Node
category val children =
    Category { title = val, children = children }


header : String -> Node
header val =
    Header { title = val }


syse : List Node
syse =
    [ category "Appearance"
        [ kcm "Global Theme"
        , kcm "Application Style"
        , kcm "Plasma Style"
        , kcm "Colors"
        , kcm "Window Decorations"
        , kcm "Fonts"
        , kcm "Icons"
        , kcm "Cursors"
        , kcm "Launch Feedback"
        , kcm "Splash Screen"
        ]
    , header "Workspace"
    , category "Workspace Behaviour"
        [ kcm "General Behaviour"
        , kcm "Desktop Effects"
        , kcm "Screen Edges"
        , kcm "Touch Screen"
        , kcm "Screen Locking"
        , kcm "Virtual Desktop"
        , kcm "Activities"
        ]
    , category "Window Management"
        [ kcm "Window Behaviour"
        , kcm "Task Switcher"
        , kcm "KWin Scripts"
        , kcm "Window Rules"
        ]
    , category "Shortcuts"
        [ kcm "Shortcuts"
        , kcm "Custom Shortcuts"
        ]
    , category "Startup and Shutdown"
        [ kcm "Login Screen (SDDM)"
        , kcm "Autostart"
        , kcm "Background Services"
        , kcm "Desktop Session"
        ]
    , category "Search"
        [ kcm "File Search"
        , kcm "KRunner"
        , kcm "Web Search Keywords"
        ]
    , header "Personalization"
    , kcm "Notifications"
    , kcm "Users"
    , category "Regional Settings"
        [ kcm "Language"
        , kcm "Formats"
        , kcm "Spell Check"
        , kcm "Date & Time"
        ]
    , kcm "Accessibility"
    , category "Applications"
        [ kcm "File Associations"
        , kcm "Locations"
        , kcm "Default Applications"
        ]
    , kcm "KDE Wallet"
    , kcm "Online Accounts"
    , kcm "User Feedback"
    , header "Network"
    , kcm "Connections"
    , category "Network Settings"
        [ kcm "Proxy"
        , kcm "Connection Preferences"
        , kcm "SSL Preferences"
        , kcm "Cache"
        , kcm "Cookies"
        , kcm "Browser Identification"
        , kcm "Windows Shares"
        ]
    , header "Hardware"
    , category "Input Devices"
        [ kcm "Keyboard"
        , kcm "Mouse"
        , kcm "Game Controller"
        , kcm "Touchpad"
        , kcm "Virtual Keyboard"
        ]
    , category "Display and Monitor"
        [ kcm "Display Configuration"
        , kcm "Compositor"
        , kcm "Night Color"
        ]
    , kcm "Audio"
    , category "Power Management"
        [ kcm "Energy Saving"
        , kcm "Activity Power Settings"
        , kcm "Advanced Power Settings"
        ]
    , kcm "Bluetooth"
    , kcm "Color Corrections"
    , kcm "KDE Connect"
    , kcm "Printers"
    , category "Removable Storage"
        [ kcm "Device Actions"
        , kcm "Digital Camera"
        , kcm "Removable Devices"
        ]
    , header "System Administration"
    , kcm "System Information"
    , kcm "Systemd"
    , kcm "Updates"
    ]

revised : List Node
revised =
    [ header "Appearance"
    , kcm "Global Theme"
    , kcm "Application Style"
    , kcm "Plasma Style"
    , kcm "Colors"
    , kcm "Window Decorations"
    , kcm "Fonts"
    , kcm "Icons"
    , kcm "Cursors"
--  , kcm "Launch Feedback"
    , kcm "Splash Screen"
    , header "Workspace" -- regarded as jargon
    , category "Workspace Behaviour" -- regarded as jargon
        [ kcm "General Behaviour" -- regarded as jargon
        , kcm "Desktop Effects" -- regarded as jargon
        , kcm "Screen Edges" -- regarded as jargon
        , kcm "Touch Screen" -- regarded as jargon
        , kcm "Screen Locking" -- regarded as jargon
        , kcm "Virtual Desktop" -- regarded as jargon
        , kcm "Activities" -- regarded as jargon
        ]
    , category "Window Management" -- regarded as jargon
        [ kcm "Window Behaviour" -- regarded as jargon
        , kcm "Task Switcher" -- regarded as jargon
        , kcm "KWin Scripts" -- regarded as jargon
        , kcm "Window Rules" -- regarded as jargon
        ]
    , category "Shortcuts"
        [ kcm "Shortcuts"
        , kcm "Custom Shortcuts"
        ]
    , category "Startup and Shutdown"
        [ kcm "Login Screen (SDDM)"
        , kcm "Autostart"
        , kcm "Background Services"
        , kcm "Desktop Session"
        ]
    , category "Search"
        [ kcm "File Search"
        , kcm "KRunner" -- regarded as jargon
        , kcm "Web Search Keywords"
        ]
    , header "Regional Settings"
    , kcm "Language"
    , kcm "Formats"
    , kcm "Spell Check"
    , kcm "Date & Time"
    , header "Personalization"
    , kcm "Notifications"
    , kcm "Users"
    , kcm "Accessibility"
    , category "Applications"
        [ kcm "File Associations"
        , kcm "Locations"
        , kcm "Default Applications"
        ]
    , kcm "KDE Wallet"
    , kcm "Online Accounts"
    , kcm "User Feedback"
    , header "Network"
    , kcm "Connections"
    , kcm "Windows Shares" -- regarded as jargon
    -- , category "Network Settings" -- regarded as jargon
    --     [ kcm "Proxy" -- regarded as jargon
    --     -- can probably get rid of ^
    --     , kcm "Connection Preferences" -- regarded as jargon
    --     -- can probably get rid of ^
    --     , kcm "SSL Preferences" -- regarded as jargon
    --     -- can probably get rid of ^
    --     , kcm "Cache" -- regarded as jargon
    --     -- can probably get rid of ^
    --     , kcm "Cookies" -- regarded as jargon
    --     -- can probably get rid of ^
    --     , kcm "Browser Identification" -- regarded as jargon
    --     -- can probably get rid of ^
    --     ]
    , header "Hardware"
    , category "Input Devices"
    -- could be flattened, bringing all the items up to the root level?
        [ kcm "Keyboard"
        , kcm "Mouse"
        , kcm "Game Controller"
        , kcm "Touchpad"
        , kcm "Virtual Keyboard"
        ]
    , category "Display and Monitor"
        [ kcm "Display Configuration"
        , kcm "Compositor" -- regarded as jargon
    -- this is mostly technical mumblejumble, do we really need a GUI for it?
    -- anyone that can adjust the settings and understand what happens is probably
    -- competent with a command line
        , kcm "Night Color"
        ]
    , kcm "Audio"
    , category "Power Management"
        [ kcm "Energy Saving"
        , kcm "Activity Power Settings"
        , kcm "Advanced Power Settings"
    -- competent with a command line
        ]
    , kcm "Bluetooth"
    , kcm "Color Corrections"
    -- could be part of display settings
    , kcm "KDE Connect"
    -- does this really need to be part of system settings? seems like
    -- it should be its own app
    , kcm "Printers"
    , category "Removable Storage"
        [ kcm "Device Actions"
        -- sorely in need of an overhaul ^
        , kcm "Digital Camera"
        , kcm "Removable Devices"
        -- sorely in need of an overhaul ^
        ]
    , header "System Administration"
    , kcm "System Information"
    , kcm "Systemd"
    -- should be its own app ^
    , kcm "Updates"
    ]


-- MAIN


main : Program () Model Msg
main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlChange = UrlChanged
        , onUrlRequest = LinkClicked
        }



-- MODEL



type alias Task =
    { item : String
    , description : String
    }


task : String -> String -> Task
task it description =
    { item = it, description = description }

tasks : Array.Array Task
tasks =
    Array.fromList
        [ task "-" "You will be presented scenarios on the right side of the screen, over here. Your task is to click the item that you think contains the solution to the scenario. What's being tested here is the sorting of items, not you. If you get confused or can't find an item to solve a task with, that's the fault of how we sorted the things, not your fault. If you can't figure out the solution, click the 'I don't know' button that will be displayed when you start testing instead of clicking random items until you find the right one. To select an item, click on it. Clicking on an item with an > on the right side will reveal more items. The currently selected item will be highlighted in blue. This will be the item submitted when you click 'Next Task.'"
        , task "Users" "Your aunt just got a new computer, and wants you to set it up for her family. Where do you go to make accounts on the computer for them?"
        , task "Date & Time" "You want to change the system time from 12-hour time to 24-hour time. Where do you go to make your system say 13:00 instead of 1:00 PM?"
        , task "Window Decorations" "You want to put the close button on windows on the left side instead of the right side. Where do you go to do this?"
        , task "Connections" "You just arrived at your hotel and need to connect to the WiFi to check emails. How do you connect to WiFi?"
        , task "Fonts" "The text on your screen is too small. How do you make it bigger?"
        , task "Launch Feedback" "There's a small bouncing picture by your pointer whenever you open an app. This annoys you. Where do you go to get rid of it?"
        ]

revisedTasks : Array.Array Task
revisedTasks =
    Array.fromList
        [ task "-" "You will be presented scenarios on the right side of the screen, over here. Your task is to click the item that you think contains the solution to the scenario. What's being tested here is the sorting of items, not you. If you get confused or can't find an item to solve a task with, that's the fault of how we sorted the things, not your fault. If you can't figure out the solution, click the 'I don't know' button that will be displayed when you start testing instead of clicking random items until you find the right one. To select an item, click on it. Clicking on an item with an > on the right side will reveal more items. The currently selected item will be highlighted in blue. This will be the item submitted when you click 'Next Task.'"
        , task "Users" "Your aunt just got a new computer, and wants you to set it up for her family. Where do you go to make accounts on the computer for them?"
        , task "Date & Time" "You want to change the system time from 12-hour time to 24-hour time. Where do you go to make your system say 13:00 instead of 1:00 PM?"
        , task "Window Decorations" "You want to put the close button on windows on the left side instead of the right side. Where do you go to do this?"
        , task "Connections" "You just arrived at your hotel and need to connect to the WiFi to check emails. How do you connect to WiFi?"
        , task "Fonts" "The text on your screen is too small. How do you make it bigger?"
        , task "Cursors" "There's a small bouncing picture by your pointer whenever you open an app. This annoys you. Where do you go to get rid of it?"
        ]

currentTasks = revisedTasks
currentArrangement = revised

type alias Model =
    { categories : List Node
    , currentCategory : Maybe Node
    , task : Int
    , msgs : List ( LogMsg, Time.Posix )
    , currentItem : String
    }


init : () -> Url.Url -> Nav.Key -> ( Model, Cmd Msg )
init _ _ _ =
    ( { categories = currentArrangement, currentCategory = Nothing, task = 0, msgs = [], currentItem = "" }, Cmd.none )



-- UPDATE


type LogMsg
    = HoveredItem String
    | UnhoveredItem String
    | ClickedItem String
    | HoveredCategory String
    | UnhoveredCategory String
    | ClickedCategory String
    | HoveredHeader String
    | UnhoveredHeader String
    | ClickedHeader String
    | UserGaveUp
    | UserWentNext
        { selected : String
        , actual : String
        }


aType : String -> ( String, Encode.Value )
aType it =
    ( "type", Encode.string it )


item : String -> ( String, Encode.Value )
item it =
    ( "item", Encode.string it )


encodeMessage : ( LogMsg, Time.Posix ) -> Encode.Value
encodeMessage msg =
    let
        data =
            case Tuple.first msg of
                HoveredItem it ->
                    [ aType "hoveredItem", item it ]

                UnhoveredItem it ->
                    [ aType "unhoveredItem", item it ]

                ClickedItem it ->
                    [ aType "clickedItem", item it ]

                HoveredCategory it ->
                    [ aType "hoveredCategory", item it ]

                UnhoveredCategory it ->
                    [ aType "unhoveredCategory", item it ]

                ClickedCategory it ->
                    [ aType "clickedCategory", item it ]

                HoveredHeader it ->
                    [ aType "hoveredHeader", item it ]

                UnhoveredHeader it ->
                    [ aType "unhoveredHeader", item it ]

                ClickedHeader it ->
                    [ aType "clickedHeader", item it ]

                UserGaveUp ->
                    [ aType "userGaveUp" ]

                UserWentNext dat ->
                    [ aType "userWentNext", ( "selected", Encode.string dat.selected ), ( "actual", Encode.string dat.actual ) ]
    in
    Encode.object (data ++ [ ( "time", Encode.int (Time.posixToMillis (Tuple.second msg)) ) ])


type Msg
    = LinkClicked Browser.UrlRequest
    | UrlChanged Url.Url
    | CategoryClicked Node
    | ItemClicked String
    | GaveUp
    | Log LogMsg
    | DoLog LogMsg Time.Posix
    | Download
    | NextTask


tag model msg =
    if model.task == 0 then
        Cmd.none

    else
        case Array.get model.task currentTasks of
            Just _ ->
                Task.perform (\a -> DoLog msg a) Time.now

            Nothing ->
                Cmd.none


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        LinkClicked _ ->
            ( model, Cmd.none )

        UrlChanged _ ->
            ( model, Cmd.none )

        CategoryClicked it ->
            case it of
                Category cat ->
                    ( { model | currentCategory = Just it }, tag model (ClickedCategory cat.title) )

                _ ->
                    ( model, Cmd.none )

        ItemClicked it ->
            case Array.get model.task currentTasks of
                Nothing ->
                    ( model, Cmd.none )

                Just a ->
                    ( { model | currentItem = it }, tag model (ClickedItem it) )

        GaveUp ->
            ( { model | task = model.task + 1, currentItem = "", currentCategory = Nothing }, tag model UserGaveUp )

        Log logMsg ->
            ( model, tag model logMsg )

        NextTask ->
            case Array.get model.task currentTasks of
                Nothing ->
                    ( model, Cmd.none )

                Just a ->
                    ( { model | task = model.task + 1, currentItem = "", currentCategory = Nothing }
                    , tag model
                        (UserWentNext
                            { selected = model.currentItem
                            , actual = a.item
                            }
                        )
                    )

        DoLog logMsg time ->
            ( { model | msgs = ( logMsg, time ) :: model.msgs }, Cmd.none )

        Download ->
            ( model, File.Download.string "data.json" "application/json" (Encode.encode 4 (Encode.list encodeMessage model.msgs)) )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none



-- VIEW


icon : String -> Html msg
icon name =
    span [ class "material-icons" ] [ text name ]


kcmItem : Model -> String -> Html Msg
kcmItem model name =
    div [ classList [ ( "list-item", True ), ( "selected-item", model.currentItem == name ) ], onClick (ItemClicked name), onMouseOver (Log (HoveredItem name)), onMouseOut (Log (UnhoveredItem name)) ] [ span [] [ text name ] ]


categoryItem : String -> Node -> Html Msg
categoryItem name node =
    div [ class "list-item", onClick (CategoryClicked node), onMouseOver (Log (HoveredCategory name)), onMouseOut (Log (UnhoveredCategory name)) ] [ span [] [ text name ], icon "chevron_right" ]


headerItem : String -> Html Msg
headerItem name =
    div [ classList [ ( "list-item", True ), ( "header-item", True ) ], onClick (Log (ClickedHeader name)), onMouseOver (Log (HoveredHeader name)), onMouseOut (Log (UnhoveredHeader name)) ] [ span [] [ text name ] ]


renderCategory : Model -> Node -> Html Msg
renderCategory model cat =
    case cat of
        KCM node ->
            kcmItem model node.title

        Header node ->
            headerItem node.title

        Category node ->
            categoryItem node.title cat


renderCategories : Model -> List Node -> Html Msg
renderCategories model cat =
    div [ class "categories" ] (List.map (\it -> renderCategory model it) cat)


separator : Html msg
separator =
    div [ class "separator" ] []


css : String -> Html msg
css path =
    node "link" [ rel "stylesheet", href path ] []


maybeHtml val do =
    case val of
        Just x ->
            do x

        Nothing ->
            text ""


maybeHtmlOr val do whenNot =
    case val of
        Just x ->
            do x

        Nothing ->
            whenNot


ifHtml cond do =
    if cond then
        do

    else
        text ""


congrats model =
    div []
        [ p [] [ text "You are done!" ]
        , p [] [ text "Press the Download button and send the file to Janet:" ]
        ]


renderSubcategory : Model -> Node -> Html Msg
renderSubcategory model node =
    case node of
        KCM _ ->
            text ""

        Header _ ->
            text ""

        Category it ->
            renderCategories model it.children


view : Model -> Browser.Document Msg
view model =
    { title = "Tree Test"
    , body =
        [ css "main.css"
        , css "https://fonts.googleapis.com/icon?family=Material+Icons"
        , div [ class "mainview" ]
            [ renderCategories model model.categories
            , separator
            , maybeHtml model.currentCategory (\it -> renderSubcategory model it)
            , maybeHtml model.currentCategory (\_ -> separator)
            ]
        , div [ class "offview" ]
            [ maybeHtmlOr (Array.get model.task currentTasks) (\it -> p [] [ text it.description ]) (congrats model)
            , ifHtml ((model.task < Array.length currentTasks) && (model.task > 0)) (button [ onClick NextTask, disabled (model.currentItem == "") ] [ text "Next Task" ])
            , button
                [ onClick
                    (if model.task >= Array.length currentTasks then
                        Download

                     else
                        GaveUp
                    )
                ]
                [ text
                    (if model.task == 0 then
                        "Start"

                     else if model.task >= Array.length currentTasks then
                        "Download"

                     else
                        "Give Up"
                    )
                ]
            ]
        ]
    }
